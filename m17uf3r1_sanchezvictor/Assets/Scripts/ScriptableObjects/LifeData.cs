using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Life", menuName = "Item/Life")]
public class LifeData : ItemData
{
    public int vidaCura; //en el caso de botiquin
    public int vidaExtra; // armaduras/cascos
}
