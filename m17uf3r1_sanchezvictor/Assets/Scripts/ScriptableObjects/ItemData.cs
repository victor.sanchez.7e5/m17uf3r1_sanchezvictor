using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Item", menuName = "Item")]
public class ItemData : ScriptableObject
{
    public string Name;
    public GameObject Prefab;
    public string Description;
    public Sprite icon;
}
