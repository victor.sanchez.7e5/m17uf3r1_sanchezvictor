using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Rayo", menuName = "Item/Rayo")]
public class RayoData : ItemData
{
    //Atributos de velocidad, fuerza, salto, etc.
}
