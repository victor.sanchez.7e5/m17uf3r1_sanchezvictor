using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Armas", menuName = "Item/Armas")]
public class WeaponData : ItemData
{
    public float Dany;
    public float Cadencia;
    public int BulletsCarregador;
    public int BulletsTotal;
}
