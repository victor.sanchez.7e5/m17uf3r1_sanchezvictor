using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 
public class Player : MonoBehaviour
{
    public int coins;
    public GameObject playerWeapon; 

    // Start is called before the first frame update
    void Start()
    {
        playerWeapon.SetActive(false);  
    }

    // Update is called once per frame
    void Update()
    {
        if (coins >= 6)
        {
            SceneManager.LoadScene("Playground"); 
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("coin"))
        {
            PlayerCollectingCoins(other); 
        }
    }

    public void PlayerCollectingCoins(Collider other)
    {
        coins++; 

        Destroy(other.gameObject);
    }

}
