using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    private CharacterController controller;
    public Transform camara;

    [Header("Estadísticas Normales")]
    [SerializeField] private float velocidad;
    [SerializeField] private float alturaDeSalto;
    [SerializeField] private float tiempoAlGirar;

    [Header("Datos sobre el piso")]
    [SerializeField] private Transform detectaPiso;
    [SerializeField] private float distanciaPiso;
    [SerializeField] private LayerMask mascaraPiso;


    float velocidadGiro;
    float gravedad = -9.81f;
    Vector3 velocity;
    bool tocaPiso;

    public Animator anim;

    private void Start()
    {
        controller = GetComponent<CharacterController>();
    }

    private void Update()
    {

        velocidad = 5;

        LaJumpa();

        if (Input.GetKey(KeyCode.LeftShift))
        {
            velocidad = 10;
            Debug.Log("RUNNING"); 
        }
        
        velocity.y += gravedad * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);

        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        if(distanciaPiso < 0.5f) anim.SetFloat("velocity", velocidad);

        Vector3 direccion = new Vector3(horizontal, 0f, vertical).normalized;

        if (direccion.magnitude >= 0.1f)
        {
            float objetivoAngulo = Mathf.Atan2(direccion.x, direccion.z) * Mathf.Rad2Deg + camara.eulerAngles.y;
            float angulo = Mathf.SmoothDampAngle(transform.eulerAngles.y, objetivoAngulo, ref velocidadGiro, tiempoAlGirar);
            transform.rotation = Quaternion.Euler(0, angulo, 0);

            Vector3 mover = Quaternion.Euler(0, objetivoAngulo, 0) * Vector3.forward;
            controller.Move(mover.normalized * velocidad * Time.deltaTime); 
        }
        else
            anim.SetFloat("velocity", 0);
    }

    /// <summary>
    /// Salt del Jugador
    /// </summary>
    /// <returns></returns>
    public int LaJumpa()
    {
        tocaPiso = Physics.CheckSphere(detectaPiso.position, distanciaPiso, mascaraPiso);

        if (tocaPiso && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        if (Input.GetButtonDown("Jump") && tocaPiso)
        {
            
            anim.SetBool("jump", true);
            StartCoroutine(EsperaQueSalta()); 

        }
        return 0; 
    }

    public void LaJumpaTrue()
    {
        velocity.y = Mathf.Sqrt(alturaDeSalto * -2 * gravedad);
    }

    IEnumerator EsperaQueSalta()
    {
        yield return new WaitForSeconds(1.5f);
        anim.SetBool("jump", false);
    }

}
