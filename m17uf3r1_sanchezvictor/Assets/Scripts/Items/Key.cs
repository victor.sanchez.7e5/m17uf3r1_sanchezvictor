using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour
{
    public GameObject key;
    bool keyEquiped = true; 

    public bool OpenDoor()
    {
        return keyEquiped;
    }

    public void PickKey()
    {
        key.SetActive(true);
        keyEquiped = true;
    }

    public void AnyObjectOnHand()
    {
        key.SetActive(false); 
    }

}
