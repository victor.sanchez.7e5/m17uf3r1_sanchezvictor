using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Item : Inventory, IRecolectable
{
    public GameObject _UItem;
    //[HideInInspector]
    public GameObject itemObjectonScene;    //GameObject amb el Box Colider
    public bool _itemEquipable ;

    [HideInInspector]
    public ItemData _item; 
    public ItemData[] randomItem = new ItemData[3];     //Array de Posibles Items que pueden hacer Spawn en el Objeto ITEM
    GameObject itemInstance; 

    /// <summary>
    /// Dintre d'un Array d'Scriptable Objects amb els possibles Items que podem agafar, assignem de forma aleatoria
    /// un Objecte per instanciar al spawner d'Items d'on el jugador els podr� recolectar
    /// </summary>
    public void SelectRandomItem()
    {
        _item  = randomItem[Random.Range(0, 3)];
        itemInstance =  Instantiate(_item.Prefab, itemObjectonScene.transform.position, Quaternion.identity);     //Instanciamos el Prefab que le pasamos al Scriptable Object
        itemInstance.SetActive(true); 
    }

    public void Start()
    {
        _UItem.SetActive(false); 
        SelectRandomItem();
        _itemEquipable = false; //no tiene el arma equipada al empezar
    }

    //METODES Interface |
    /// <summary>
    /// Afegeix l'Scriptable object al inventari  i destruim l'Spawner de Items de l'Escena
    /// </summary>
    /// <param name="item"></param>
    public void Recoger(string item/*, int cantidad*/)
    {
        Debug.Log($"Item {item} collected");
        inventory.Add(_item);       //Afegim a la llista de Scriptable Objects del Inventari l'Scriptable Object que tenim Instanciat al Joc

        Destroy(itemObjectonScene);
        Destroy(itemInstance);    //nom del ITEM
    }

    public bool ItemEquipable()
    {
        return _itemEquipable;
    }

    //                  ^
    //METODES Interface |


    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == ("Player"))
        {
            _UItem.SetActive(true);
        }
    }

    public void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                //_itemEquipable = true;
                //_UItem.SetActive(true);
                Player.GetComponent<Inventory>().AddToInventory(_item);

                Recoger(_item.name);     //Metode de la Interficie 
            }
        }
    }

    public void OnDestroy()
    {
        _UItem.SetActive(false);
    }

    public void OnTriggerExit(Collider other)
    {
        _UItem.SetActive(false); 
    }
   
}


