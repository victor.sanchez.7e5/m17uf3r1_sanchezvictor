using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public List<ItemData> inventory = new List<ItemData>();

    //public Image imgInventory1;
    //public Image imgInventory2;
    //public Image imgInventory3;
    //public Image imgInventory4;
    public Image[] slots;
    public GameObject Player;


    void Start()
    {
        slots[0].sprite = null;
        slots[1].sprite = null;
        slots[2].sprite = null;
        slots[3].sprite = null;

    }

    /// <summary>
    /// Metode per a que la Classe Item afegeixi Items a l'inventari del Jugador (chamberPlayer)
    /// </summary>
    /// <param name="newItem"></param>
    public void AddToInventory(ItemData newItem)
    {
        inventory.Add(newItem);
        //slots[1].sprite = newItem.icon;
        for (int i = 0; i < slots.Length; i++)
        {
            if (slots[i].sprite == null)
            {
                slots[i].sprite = newItem.icon;
                break;
            }
            //else
            //{
            //}
        }
    }

    void Update()
    {
        PickInventoryItem();

    }

    /// <summary>
    /// Metode que segons el input que reb selecciona un element o altre de l'inventari
    /// </summary>
    public void PickInventoryItem()
    {
        int actualWeapon = 0;

        // Seleccionar arma con teclado num�rico
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            actualWeapon = 0;
            SelectWeapon(actualWeapon);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            /*if(inventory[0].name == "Vandal") Player.GetComponent<Weapon>().PickWeapon();*/       //crida al arma de prova
                                                                                                    //s'hauria de crear el metode Pick  
                                                                                                    //a la classe ITEM per poder agafar l'item corresponent
            actualWeapon = 1;
            SelectWeapon(actualWeapon);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            actualWeapon = 2;
            SelectWeapon(actualWeapon);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            actualWeapon = 3;
            SelectWeapon(actualWeapon);
        }
    }

    public void SelectWeapon(int actualWeapon)
    {
        //Desactivar todos los items, para que , el item actualmente equipado lo desactivemos. 
        for (int i = 0; i < inventory.Count; i++)
        {
            inventory[i].Prefab.SetActive(false);
        }
        //Player.GetComponent<Weapon>().AnyObjectOnHand();    //Organizar Estos Metodos en el Player
        //Player.GetComponent<Key>().AnyObjectOnHand();    //Organizar Estos Metodos en el Player

        // Activar el item seleccionado teniendo en cuenta el nombre recibido en el SO : 
        switch (inventory[actualWeapon].name)
        {
            case "Vandal":
                Player.GetComponent<Weapon>().PickVandal();
                break;
            case "Key":
                Debug.Log("KeyEquiped"); 
                Player.GetComponent<Key>().PickKey();
                break;
            case "Rambo":
                Player.GetComponent<Weapon>().PickKnife();
                break;
            case "None":
                break;
        }
        inventory[actualWeapon].Prefab.SetActive(true);
    }

}
