    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRecolectable
{

    public void Recoger(string item/*, int cantidad*/);

    public bool ItemEquipable(); //Activar el Item en la UI de inventario
    
}
