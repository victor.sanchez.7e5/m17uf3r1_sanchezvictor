using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI; 
public class EnemyAI : MonoBehaviour
{
    public NavMeshAgent agent;

    public int health; //Esbrinar si es la del player o Enemy

    public Transform player;

    public LayerMask layerGround, layerPlayer;

    public GameObject projectile; 
    //Patrol
    public Vector3 walkPoint;
    public bool walkPointSet;
    public float walkPointRange; 

    //Atack
    public float timeBetweenAttacks;
    bool alreadyAttacked;

    //States 
    public float visionRange, atackRange;
    public bool playerInVisionRange, playerInAttackRange;

    private void Awake()
    {
        player = GameObject.Find("ChambPlayer").transform;
        agent = GetComponent<NavMeshAgent>();
    }

    private void Update()
    {
        playerInVisionRange = Physics.CheckSphere(transform.position, visionRange, layerPlayer);  
        playerInAttackRange = Physics.CheckSphere(transform.position, atackRange, layerPlayer);

        if (!playerInVisionRange && !playerInAttackRange) Patroling();
        if (playerInVisionRange && !playerInAttackRange) ChasePlayer();
        if (playerInVisionRange && playerInAttackRange) AttackPlayer(); 
    }

    private void Patroling()
    {
        if (!walkPointSet) SearchWalkPoint();

        if (walkPointSet)
            agent.SetDestination(walkPoint);

        Vector3 distanceToWalkPoint = transform.position - walkPoint;

        //Walkpoint Reached 
        if (distanceToWalkPoint.magnitude < 1f)
            walkPointSet = false; 
    }

    private void SearchWalkPoint()
    {
        float randomZ = Random.Range(-walkPointRange, walkPointRange); 
        float randomX = Random.Range(-walkPointRange, walkPointRange);

        walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);
        walkPointSet = true;

        if (Physics.Raycast(walkPoint, -transform.up, 2f, layerGround)) walkPointSet = true;

    }

    private void ChasePlayer()
    {
        agent.SetDestination(player.position); 
    }
    private void AttackPlayer()
    {
        agent.SetDestination(transform.position);

        transform.LookAt(player);

        if (!alreadyAttacked)
        {
            //Attack 
            Rigidbody rb = Instantiate(projectile, transform.position, Quaternion.identity).GetComponent<Rigidbody>();
            rb.AddForce(transform.forward* 32f , ForceMode.Impulse);
            rb.AddForce(transform.up * 8f, ForceMode.Impulse);


            alreadyAttacked = true;
            Invoke(nameof(ResetAttack), timeBetweenAttacks);
        }
    }

    private void ResetAttack()
    {
        alreadyAttacked = false; 
    }

    public void TakeDamage(int damage)
    {
        health -= damage; 

        if(health <= 0) Invoke(nameof(DestroyEnemy), 0.5f);
    }

    private void DestroyEnemy()
    {
        Destroy(gameObject); 
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, atackRange);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, visionRange);
    }
}
