using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.UI; 

public class Door : MonoBehaviour
{
    public Animator doorAnim;
    public GameObject _UIDoor;
    public GameObject player; 

    public void Start()
    {
        _UIDoor.SetActive(false); 
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == ("Player"))
        {
            _UIDoor.SetActive(true);
        }
    }

    public void OnTriggerStay(Collider other)
    {
        doorAnim.SetBool("doorClose", false);

        if (other.CompareTag("Player"))
        {
            if (player.GetComponent<Key>().OpenDoor()&&Input.GetKeyDown(KeyCode.E))
            {
                if(player.GetComponent<Key>().OpenDoor() && Input.GetKeyDown(KeyCode.C))
                {
                    doorAnim.SetBool("doorClose", true);
                    //doorAnim.SetBool("doorOpen", false);

                }
                doorAnim.SetBool("doorOpen", true);
            }
        }
    }

    public void OnTriggerExit(Collider other)
    {
        _UIDoor.SetActive(false); 
    }

}
