using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    /// <summary>
    /// Aquest Script ha d'estar al Player perque sino al estar desactivat no funciona el seu codi : 
    /// </summary>
    public GameObject weapon;
    public GameObject knife; 
    public Animator playerAnim;

    public void Start()
    {
        weapon.SetActive(false);
        knife.SetActive(false); 
    }

    //Gestionar Quina arma tenim equipada amb SetActive i la animaci� que li correspon
    public void PickVandal()
    {
        weapon.SetActive(true);
        playerAnim.SetBool("gunEquip", true);
        Debug.Log("Arma Equipauu");
    }

    //Gestionar Quina arma tenim equipada amb SetActive i la animaci� que li correspon
    public void PickKnife()
    {
        knife.SetActive(true);
        playerAnim.SetBool("knifeEquip", true);
        Debug.Log("Rambo Equipauu");
    }

    public void AnyObjectOnHand()
    {
        weapon.SetActive(false);
        playerAnim.SetBool("gunEquip", false);
        knife.SetActive(false);
        playerAnim.SetBool("knifeEquip", false);
    }
}
