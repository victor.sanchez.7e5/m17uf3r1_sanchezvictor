using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootController : MonoBehaviour
{
    // Prefab de la bala : 
    [SerializeField]
    private GameObject bulletPrefab;

    // Lugar donde se instancia la bala
    [SerializeField]
    private Transform firePoint;

    [SerializeField]
    public Animator PlayerAnim;

    public float bulletSpeed = 5f;
    public float cadencia = 0.1f;
    private float nextFireTime = 0f;

    void Update()
    {
        ShootBullet();
        Debug.Log("Apuntando"); 
    }

    public void ShootBullet()
    {
        if (Input.GetMouseButton(0) && Time.time > nextFireTime)
        {
            nextFireTime = Time.time + cadencia;

            GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);

            Rigidbody bulletRigidbody = bullet.GetComponent<Rigidbody>();
            bulletRigidbody.velocity = firePoint.position * bulletSpeed;
            PlayerAnim.SetFloat("shooting", 0.5f);
            Debug.Log("DISPARANDO");

        }
        else StartCoroutine(ShootAnim());
    }
        /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    IEnumerator ShootAnim()
    {
        yield return new WaitForSeconds(0.2f);
        PlayerAnim.SetFloat("shooting", 0f);
    }
}
